# warrenio/go-client #
warrenio/go-client is a Go client library for accessing the Warren platform API.

Currently, **warrenio/go-client requires Go version 1.18 or greater**.

## Installation ##
Create a Go file that imports the client.
```go
package main

import (
	w "gitlab.com/warrenio/library/go-client/warren"
	"log"
)

func main() {
	api, err := (&w.ClientBuilder{}).
		ApiUrl("<API_URL>").
		ApiToken("<API_TOKEN>").
		LocationSlug("<LOCATION_SLUG>").
		Build()
	log.Printf("%+v, %+v", api, err)
}
```

Use the `go mod` tool to download the dependency.

```bash
go mod tidy
```

## Usage ##
Minimal example:
```go
package main

import (
	w "gitlab.com/warrenio/library/go-client/warren"
	"log"
)

func main() {
	api, err := (&w.ClientBuilder{}).
		ApiUrl("https://<SOME_API_HOST>/").
		ApiToken("<API_TOKEN>").
		Build()
	if err != nil {
		log.Fatal("Failed to create client, ", err)
	}

	vm_uuid := "6374801f-85d5-45ea-944c-b006a6f4da01"
	vm, err := api.VirtualMachine.GetByUuid(vm_uuid)
	if err != nil {
		log.Printf("Failed to get VM, %+v\n", err)
	}
	log.Printf("%+v", vm)

	disk, err := api.BlockStorage.CreateDisk(&w.CreateDiskRequest{
		SizeGb: w.New(20),
	})
	if err != nil {
		log.Printf("Failed to create disk, %+v\n", err)
	}
	log.Printf("%+v", disk)
}
```
See examples directory for more information.

## License ##
Apache 2.0 License
