package main

import (
	w "gitlab.com/warrenio/library/go-client/warren"
	"log"
	"os"
)

func main() {
	// Build client object
	api, err := (&w.ClientBuilder{}).
		ApiUrl(os.Getenv("API_URL")).
		ApiToken(os.Getenv("API_TOKEN")).
		Build()
	if err != nil {
		log.Fatal("Cannot build client, ", err)
	}

	// Get the list of possible resource locations
	locs, err := api.Location.ListLocations()
	if err != nil {
		log.Fatal("Failed to get locations, ", err)
	}

	// Iterate over all locations
	for _, loc := range *locs {
		// All subsequent usages of api will now access the specified location
		api.LocationSlug = loc.Slug
		// Get the list of Floating IP addresses
		fips, err := api.Network.ListFloatingIps()
		if err != nil {
			log.Fatalf("Failed to get Floating IPs from %s, %+v", loc.Slug, err)
		}
		// Get the list of Virtual Machines
		vms, err := api.VirtualMachine.ListVms()
		if err != nil {
			log.Fatalf("Failed to get Virtual Machines from %s, %+v", loc.Slug, err)
		}
		// Print info about resources in the location
		log.Printf("%s [%s]: %d Floating IPs, %d Virtual Machines", loc.DisplayName, loc.Slug, len(*fips), len(*vms))
	}
}
