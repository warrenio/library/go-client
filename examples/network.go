package main

import (
	u "gitlab.com/warrenio/library/go-client/examples/utils"
	w "gitlab.com/warrenio/library/go-client/warren"
	"log"
	"os"
	"strconv"
)

func main() {
	// Build client object
	api, err := (&w.ClientBuilder{}).
		ApiUrl(os.Getenv("API_URL")).
		ApiToken(os.Getenv("API_TOKEN")).
		LocationSlug("tll").
		Build()
	billingAccountId, _ := strconv.Atoi(os.Getenv("BA_ID"))
	if err != nil {
		log.Fatal("Cannot build client, ", err)
	}

	VPCTesting(api)

	LoadBalancerTesting(api, billingAccountId)

}

func VPCTesting(api *w.Client) {

	// Get Default network
	defNet, err := api.Network.CreateNetwork("")
	if err != nil {
		log.Fatal("Failed to get or create default network, ", err)
	}
	log.Printf("Default network or new: %s", defNet)

	listedNets, err := api.Network.ListNetworks()
	if err != nil {
		log.Fatal("Failed to get network list", err)
	}
	log.Printf("Networks: %s", u.PrettyPrintObject(listedNets))

	oneNet, err := api.Network.GetNetworkByUUID(defNet.Uuid)
	if err != nil {
		log.Fatal("Failed to get network by UUID", err)
	}
	log.Printf("Default network: %s", u.PrettyPrintObject(oneNet))
}

func LoadBalancerTesting(api *w.Client, billingAccountId int) {

	createdLB, err := api.Network.CreateLoadBalancer(
		&w.LoadBalancerRequest{
			DisplayName:      w.New("TestGoClient"),
			BillingAccountId: w.New(billingAccountId),
			NetworkUuid:      w.New("61671bef-6590-4489-9e0c-61cfb28c56f5"),
			ReservePublicIp:  w.New(true),
			Rules:            w.New([]w.LBPortRulesRequest{{w.New(80), w.New(80)}}),
			Targets: w.New([]w.LBTargetRequest{{
				w.New("9f3fde60-c19a-4314-94c3-272645439ad7"),
				w.New("vm"),
			}}),
		})
	if err != nil {
		log.Fatal("Failed to create load balancer, ", err)
	}
	log.Println("================================================================")
	log.Printf("Created new load balancer:\n %s", u.PrettyPrintObject(createdLB))
	log.Println("================================================================")

	createdLB.DisplayName = w.New("LB with new name")
	updatedLB, err := api.Network.UpdateLoadBalancer(createdLB.Uuid, &w.LoadBalancerRequest{
		DisplayName: w.New("LB with new name"),
	})
	if err != nil {
		log.Fatal("Failed to update load balancer, ", err)
	}
	log.Println("================================================================")
	log.Printf("Updated load balancer [%s] name:\n %s", createdLB.Uuid, u.PrettyPrintObject(updatedLB))
	log.Println("================================================================")

	listAllLoadBalancers := true
	listedLBs, err := api.Network.ListLoadBalancers(listAllLoadBalancers)
	if err != nil {
		log.Fatal("Failed to list load balancers, ", err)
	}
	log.Println("================================================================")
	if listAllLoadBalancers {
		log.Printf("List of BA [%d] active and also deleted load balancers:\n %s", billingAccountId, u.PrettyPrintObject(listedLBs))
	} else {
		log.Printf("List of BA [%d] active load balancers:\n %s", billingAccountId, u.PrettyPrintObject(listedLBs))
	}
	log.Println("================================================================")

	addedLBTarget, err := api.Network.AddLoadBalancerTarget(createdLB.Uuid, &w.LBTargetRequest{
		TargetUuid: w.New("2b278662-fff6-4b9b-aca4-4dba116ea448"),
		TargetType: w.New("vm"),
	})
	if err != nil {
		log.Fatal("Failed to add new target to load balancer: ", err)
	}
	log.Println("================================================================")
	log.Printf("Added new load balancer [%s] target: %s", createdLB.Uuid, u.PrettyPrintObject(addedLBTarget))
	log.Println("================================================================")

	err = api.Network.UnlinkLoadBalancerTarget(createdLB.Uuid, addedLBTarget.TargetUuid)
	if err != nil {
		log.Fatalf("Failed to delete target [%s] from load balancer [%s]: %s ", addedLBTarget.TargetUuid, createdLB.Uuid, err)
	}
	log.Println("================================================================")
	log.Printf("Removed target [%s] from load balancer with UUID:\n %s", addedLBTarget.TargetUuid, createdLB.Uuid)
	log.Println("================================================================")

	newLBForwardingRule, err := api.Network.AddLoadBalancerRule(createdLB.Uuid, &w.LBForwardingRuleRequest{
		SourcePort: w.New(81),
		TargetPort: w.New(430),
	})
	if err != nil {
		log.Fatalf("Failed create forwarding rule for load balancer [%s]: %s ", createdLB.Uuid, err)
	}
	log.Println("================================================================")
	log.Printf("Created new load balancer forwarding rule:\n %s", u.PrettyPrintObject(newLBForwardingRule))
	log.Println("================================================================")

	err = api.Network.DropLoadBalancerRule(createdLB.Uuid, newLBForwardingRule.Uuid)
	if err != nil {
		log.Fatalf("Failed to delete forwarding rule [%s] from load balancer [%s]: %s ",
			newLBForwardingRule.Uuid, createdLB.Uuid, err)
	}
	log.Println("================================================================")
	log.Printf("Removed forwarding rule [%s] from load balancer with UUID:\n %s", newLBForwardingRule.Uuid, createdLB.Uuid)
	log.Println("================================================================")

	newBaId := 129863
	updatedLB, err = api.Network.ChangeLoadBalancerBillingAccount(createdLB.Uuid, newBaId)
	if err != nil {
		log.Fatalf("Failed to change BA for load balancer [%s]: %s ", createdLB.Uuid, err)
	}
	log.Println("================================================================")
	log.Printf("Changed BA [%d] to [%d] for load balancer:\n %s",
		createdLB.BillingAccountId, newBaId, u.PrettyPrintObject(updatedLB))
	log.Println("================================================================")

	// TODO: at the moment Floating IP is not deleted, needs another API call to handle this
	err = api.Network.DeleteLoadBalancer(createdLB.Uuid)
	if err != nil {
		log.Fatalf("Failed to delete load balancer [%s]: %s ", createdLB.Uuid, err)
	}
	log.Println("================================================================")
	log.Printf("Deleted load balancer with UUID:\n %s", createdLB.Uuid)
	log.Println("================================================================")

}
