package utils

import "encoding/json"

// PrettyPrintObject Print object in JSON format
func PrettyPrintObject(defLb any) []byte {
	out, _ := json.MarshalIndent(defLb, "", "\t")
	return out
}
