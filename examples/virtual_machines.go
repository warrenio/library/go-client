package main

import (
	"fmt"
	u "gitlab.com/warrenio/library/go-client/examples/utils"
	w "gitlab.com/warrenio/library/go-client/warren"
	"log"
	"net"
	"os"
	"strconv"
)

func main() {
	apiUrl := os.Getenv("API_URL")
	apiToken := os.Getenv("API_TOKEN")
	// Build client object
	api, err := (&w.ClientBuilder{}).
		ApiUrl(apiUrl).
		ApiToken(apiToken).
		//		LocationSlug("...").
		Build()
	if err != nil {
		log.Fatal("Cannot build client, ", err)
	}

	billingAccountId, _ := strconv.Atoi(os.Getenv("BA_ID"))

	// First let's see which Ubuntu versions are available
	base, err := api.VirtualMachine.ListBaseImages()
	if err != nil {
		log.Fatal("Could not list base images, ", err)
	}
	var ubuntu *w.BaseImage
	for _, os := range *base {
		if os.OsName == "ubuntu" {
			ubuntu = &os
			break
		}
	}
	if ubuntu == nil {
		log.Fatal("Seems that Ubuntu is not supported")
	}
	log.Printf("Ubuntu versions available: %+v", ubuntu.Versions)

	// Create a Ubuntu 20.04 Virtual Machine
	vm, err := api.VirtualMachine.CreateVirtualMachine(&w.CreateVirtualMachineRequest{
		Name:             w.New("My Machine"),
		OsName:           w.New("ubuntu"),
		OsVersion:        w.New("20.04"),
		Disks:            w.New(20),
		VCpu:             w.New(2),
		Ram:              w.New(2048),
		Username:         w.New("alice"),
		Password:         w.New("cd9fb1e148ccd8442E5a"),
		BillingAccountId: &billingAccountId, // No need to set Billing Account Id if the API token is already restricted to a Billing Account
		Backup:           w.New(false),
		//PublicKey:        w.New("ssh-rsa AAAAB3...== alice@home"),
		ReservePublicIp: w.New(true),
		CloudInit:       w.New(`{"write_files": [{"path": "/firstboot", "content": "IyEvYmluL2Jhc2gKZWNobyBOb3cgQm9vdGluZwoK", "permissions": "0755", "encoding": "b64"}]}`),
	})

	if err != nil {
		log.Fatal("Failed to create Virtual Machine", err)
	}

	log.Printf("VM: %+v", vm)

	// Get the public IP address of this VM
	// New public Floating IP was created together with VM because of ReservePublicIp: w.New(true)
	fips, err := api.Network.ListFloatingIps()
	if err != nil {
		log.Fatal("Failed to get Floating IPs", err)
	}
	var fip *w.FloatingIp
	for _, f := range *fips {
		if f.AssignedTo == vm.Uuid {
			fip = &f
			break
		}
	}
	log.Printf("VM %s [%s] has IP %s", vm.Name, vm.Uuid, fip.Address)

	// Add a second disk, make it a copy of the first one
	disk, err := api.BlockStorage.CreateDisk(&w.CreateDiskRequest{
		SizeGb:           w.New(25),
		BillingAccountId: &billingAccountId,
		SourceImageType:  w.New(w.DISK),
		SourceImage:      &vm.Storage[0].Uuid,
	})

	if err != nil {
		log.Fatal("Failed to create disk ", err)
	}
	log.Printf("Disk %+v", disk)

	vmStorage, err := api.VirtualMachine.AttachDisk(vm.Uuid, disk.Uuid)
	if err != nil {
		log.Fatal("Failed to attach disk to VM", err)
	}
	log.Printf("Attached disk %+v to VM %s", vmStorage, vm.Uuid)

	vm, err = api.VirtualMachine.GetByUuid(vm.Uuid)
	if err != nil {
		log.Fatal("Failed to get VM", err)
	}
	log.Printf("VM %+v", vm)

	// You can access the VM now via ssh

	/*
		 * This is what the VM now looks like. Use it as you will.
		 *
		alice@MyMachine:~$ sudo lsblk
		NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
		loop0     7:0    0 55.3M  1 loop /snap/core18/1885
		loop1     7:1    0 70.6M  1 loop /snap/lxd/16922
		loop3     7:3    0 55.6M  1 loop /snap/core18/2560
		loop4     7:4    0   47M  1 loop /snap/snapd/16292
		loop5     7:5    0   62M  1 loop /snap/core20/1611
		loop6     7:6    0 67.8M  1 loop /snap/lxd/22753
		vda     252:0    0   20G  0 disk
		├─vda1  252:1    0 19.9G  0 part /
		├─vda14 252:14   0    4M  0 part
		└─vda15 252:15   0  106M  0 part /boot/efi
		vdb     252:16   0   25G  0 disk
	*/

	unAssignedIp1, err := api.Network.UnAssignFloatingIp(net.ParseIP(fip.Address))
	if err != nil {
		log.Fatal("Failed to unassign floating IP", err)
	}
	log.Printf("Unassigned floating IP %s", u.PrettyPrintObject(unAssignedIp1))

	// Create new unassigned floating IP
	ip, err := api.Network.CreateFloatingIp(&w.CreateFloatingIpRequest{
		Name:             w.New("fip_for-" + vm.Name),
		BillingAccountId: &billingAccountId,
	})
	if err != nil {
		log.Fatal("Failed to create floating IP", err)
	}
	log.Printf("Floating IP %s", u.PrettyPrintObject(ip))

	floatingIp, err := api.Network.AssignFloatingIp(net.ParseIP(ip.Address), vm.Uuid)
	if err != nil {
		log.Fatal("Failed to assign floating IP", err)
	}
	log.Printf("Assigned floating IP %s", u.PrettyPrintObject(floatingIp))

	unAssignedIp2, err := api.Network.UnAssignFloatingIp(net.ParseIP(floatingIp.Address))
	if err != nil {
		log.Fatal("Failed to unassign floating IP", err)
	}
	log.Printf("Unassigned floating IP %s", u.PrettyPrintObject(unAssignedIp2))

	floatingIps, err := api.Network.ListFloatingIps()
	if err != nil {
		log.Fatal("Failed to list floating IPs", err)
	}
	log.Printf("Floating IPs %s", u.PrettyPrintObject(floatingIps))

	// All allocated resources will be removed after this point.
	fmt.Println("Press Enter to continue with clean up...")
	var line *string
	fmt.Scanln(line)

	// Delete the VM and the Floating IP explicitly,
	// otherwise the Floating IP will stay around, and you will be charged for it
	err = api.Network.DeleteFloatingIp(net.ParseIP(unAssignedIp1.Address))
	if err != nil {
		log.Fatal("Failed to delete Floating IP", err)
	}
	// Delete the VM and the Floating IP explicitly,
	// otherwise the Floating IP will stay around, and you will be charged for it
	err = api.Network.DeleteFloatingIp(net.ParseIP(unAssignedIp2.Address))
	if err != nil {
		log.Fatal("Failed to delete Floating IP", err)
	}

	// Do note that all the disks that are attached to a VM while the VM is being
	// deleted will be deleted as well.
	err = api.VirtualMachine.DeleteVm(vm.Uuid)
	if err != nil {
		log.Fatal("Failed to delete Virtual Machine", err)
	}
	log.Printf("Removed Virtual Machine with all its disks and Floating IP successfully")
}
